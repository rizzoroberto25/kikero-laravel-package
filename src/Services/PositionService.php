<?php
namespace Kikero\Services;
use Illuminate\Database\Eloquent\Model;

class PositionService {

    public $Model;
    public $ModelClass;
    public $position;
	public $bondPosition;
	public $bondPositionValue;
    public $bonds;

    public function __construct(Model $Model, $bonds=NULL) {
        $this->Model = $Model;
        $this->ModelClass = get_class($this->Model);
        if(!is_null($bonds)) {
            $this->setBonds($bonds);
        }
    }

    public function setBonds(array $bonds) {
        /*
        $this->bondPosition = $bonds['field'];
        $this->bondPositionValue = $bonds['value'];
        */
        $this->bonds = $bonds;
    }

    public function setPosition(int $val) {
		
		if(empty($this->Model->id)) return false;
		if(empty($val)) return false;
		
		$new_position = $val;
		$operator_search = ">=";
		if($new_position > $this->Model->position) {
			$new_position++;
			$operator_search = ">";
		}
		
        $this->Model->order = $new_position;
		$this->Model->update($values);

        $search = $this->ModelClass::where("order", $operator_search, $val)
            ->where("id", "!=", $this->Model->id);

        /*
		if(!empty($this->bondPosition) && !empty($this->bondPositionValue)) {
			
            $search = $search::where($this->bondPosition, $this->bondPositionValue);
		}
        */
        if(!empty($this->bonds) && is_array($this->bonds)) {
            foreach($this->bonds as $bondK=>$bondV) {
                $search = $search->where($bondK, $bondV);
            }
        }
		
        $res = $search->get();
		
		if(!is_null($res) && count($res)>0) {
			foreach($res as $row) {
				$row->order++;
                $row->save();
			}
		}
		$this->reorderPositions();
		
	}

    public function setElmsPositions(array $elms) {
        if(!count($elms)>0) return false;
        foreach($elms as $id=>$position) {
            if(empty($position)) continue;
            $id = (int) $id;
            $position = (int) $position;
            $this->ModelClass::where("id", $id)
                ->update(["order"=>$position, ]);
        }
        return true;
    }

    public function reorderPositions() {
        $res = $this->ModelClass::where("id", ">", 0);
        if(!empty($this->bondPosition) && !empty($this->bondPositionValue)) {
            $res = $res->where($this->bondPosition);
        }
        $res = $res->orderBy("order", "ASC")
            ->get();
        
        if($res->count()>0) {
            $position = 1;
            foreach($res as $row) {
                $row->order = $position;
                $row->save();
                $position++;
            }
        }
    }

    public function MinMaxPosition(string $aggregate) {
        if(!in_array($aggregate, ["MIN", "MAX", ])) {
            return false;
        }
        $res = $this->ModelClass::where("id", ">", 0);
        /*
        if(!empty($this->bondPosition) && !empty($this->bondPositionValue)) {
            $res = $res::where($this->bondPosition);
        }
        */
        
        if(!empty($this->bonds) && is_array($this->bonds)) {
            foreach($this->bonds as $bondK=>$bondV) {
                $res = $res->where($bondK, $bondV);
            }
        }
        if($aggregate == "MIN") {
            return $res->min("order");
        } else {
            return $res->max("order");
        }
        return false;
    }

}