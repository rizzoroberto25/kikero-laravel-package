<?php
namespace Kikero\Services;
use ResponseService;
use File;

class LanguageService {

    public $language;
    public static $csvFile = __DIR__ . "/../../resources/lang/docs/world-languages.csv";

    public static function worldLanguages() {
		$languages = array();
		$csvFile = fopen(self::$csvFile, "r");
		
		while($row = fgetcsv($csvFile, 1000, ";")) {
			$languages[$row[0]] = strtolower($row[1]);
		}
		
		fclose($csvFile);
		
		return $languages;

    }

    public static function ctlStringLanguage($language) {
		$ctl = false;
		$languages = self::worldLanguages();
		if(in_array($language, $languages)) {
			$ctl = true;
		}
		return $ctl;
	}

}