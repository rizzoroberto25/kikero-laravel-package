<?php
namespace Kikero\Services;

class Utility {

    public static function getEloquentSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    public static function codePrint($oggetto, $titolo=NULL, $visibile="block") {
	
		echo "<div style='display:".$visibile."'>";
		if(self::hasValue($titolo)) {
			
			echo "<h1>".$titolo."</h1>";
			
		}
		echo "<pre>";
		print_r($oggetto);
		
		echo "</pre>";
		echo "</div>";

		echo "<h1>DEBUG</h1>";
		echo "<pre>";
		print_r(debug_backtrace());
		echo "</pre>";
		
	}

	public static function priceStringToDecimal(string $price) {
		$price = str_replace(",", ".", $price);
		return (float)$price;
	}

	public static function isValidEmail($email):bool {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function getPreviousRouteName() {
		$url = \URL::previous();
		return app('router')->getRoutes($url)->match(app('request')->create($url))->getName();
	}
    
}