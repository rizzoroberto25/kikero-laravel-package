<?php
namespace Kikero\Services;
use App\Services\Html;
use ResponseService;

use Kikero\Models\Media;
use Kikero\Models\MediaGallery;
use ImageService;
use Position;

class MediaService extends Html {

    public $imagesMymeTypes = ["image/jpeg", "image/png", "image/gif", ];

    public function modelGallery(string $model, int $model_id) {
        $pictures = MediaGallery::with('media')
            ->where("model", $model)
            ->where("model_id", $model_id)
            ->orderBy("order")
            ->get();
        return $pictures;
    }

    public function galleryManager(string $model, int $model_id, array $route_strings, $canManage=true) {
        
        $pictures = $this->modelGallery($model, $model_id);

        
        $html = "<div class=\"row gallery-row-parent\" style=\"margin-bottom:100px;\">";

        $html .= "
        
        <div class=\"col-12\">
        
            <div class=\"card\">
                <div class=\"card-header\">
                    <strong>".__("acme::media.gallery_title")."</strong>
                </div>

                <div class=\"card-body card-block f_gallery\">
                    <div class=\"row\">
                        <div class=\"col-12 col-md-12\">
                            <div class=\"pictures_content row\" style=\"padding:20px; border:1px solid #666\">
        ";                
        
    if(count($pictures)>0) {
        foreach($pictures as $picture) {
    $html .= "
                                <div class=\"col-6 col-md-4\">
                                    <div class='picture-item' data-id='".$picture->id."' data-position='".$picture->order."'>
                                        <img src='".ImageService::getImagePath($picture->media->path, "600")."' />
    ";
            if($canManage) {
                $html .= "
                                        <div class='actions clearfix'>
                                            <div class='pull-left action'><a href='javascript:void(0)' data-delete_url='".$route_strings['delete_item']."' onclick='removeGalleryImg(this);'><i class=\"fa fa-trash trash\"></i></a></div>
                                            <div class='pull-right action'><a href='javascript:void(0)'><i class=\"fa fa-arrows\"></i></a></div>
                                        </div>
                        ";
            }

            $html .= "
                                    </div>
                                </div>
    ";
        }
    } else {
        $html .= __("acme::media.no_img_in_gallery");
    }    
         
        $html .= "  
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class=\"col-12 col-md-2\">
                        <button type=\"button\" class=\"btn btn-primary gallery_adder\" data-toggle=\"modal\" data-target=\"#AjaxModal\"><i class=\"fa fa-plus\"></i></button>	
                    </div>
                    -->
        ";
        if($canManage) {
        
            $html .= "
                    <div class=\"row\" style='padding-top:35px;'>
                        <div class='col-12 col-md-6'>
                            <form name=\"f_gallery\" class=\"parent-form f_gallery\" method=\"post\" action=\"".$route_strings['from_upload']."\" enctype=\"multipart/form-data\">
                                ".csrf_field()."
                                <div class=\"col-12\" style='padding-bottom:30px;'> 
                                    <h4>".__("acme::media.gallery_insert_images")."</h4>
                                </div>
        ";

            for($i=0; $i<3; $i++) {
                $html .= "
                                <div class=\"col-12\">
                                    <div class=\"form-group image-uploader\">
                                        <div class=\"row\">
                                            <div class=\"col-md-4\">
                                                <img src=\"".asset('img/kikero/bw-logo_segnaposto.png')."\" class=\"image_reader\" style=\"width:80px;\" />
                                            </div>
                                            <div class=\"col-md-8\">
                                                <input type=\"file\" class='image_selector' name=\"gallery_images[]\" style='margin-top:25px;' accept='image/*' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    ";
            }                                

        
        $html .= "
                                <div class=\"col-12\" style='padding-top:25px;'>
                                    <div class='row'>
                                        
                                        <div class='col-12'><button type=\"submit\" class=\"btn btn-primary btn-lg btn-block page-loader\">".__("generic.buttons.upload")."</button></div>
                                        
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    ";
        }

        $html .= "
                </div>

                <div class=\"card-footer\">
                    <!--
                    <button type=\"submit\" class=\"btn btn-success btn-sm page-loader\">
                    <i class=\"fa fa-floppy-o\"></i> Salva gallery
                    </button>
                    -->
                </div>

            </div>
        </div>
        
        ";

        $html .= "</div>";
		
        if($canManage) {
		$html .= "
			<script>
			$(document).ready(function() {
                   $('.pictures_content').sortable(

					{
						containment: '.pictures_content',
                        tolerance: 'pointer',
						stop: function(event, ui) {
							
							var elements_array = [];
							var i = 1;
							$(event.target).find('.picture-item').each(function() {
								elements_array[$(this).data('id')] = i;
								//$(this).attr('data-position', i);
								i++;
							});
							
							var post_data = {
								elements:elements_array
							};
							page_loader();
							setTimeout(function() {
								$.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')
                                    },
									url : '".$route_strings['update_positions']."',
									type: 'post',
									data: post_data,
									dataType: \"json\",
									success: function(response) {
										codePrint(response, \"Response\");
										page_loader_close();
									},
									error: function(event) {
										codePrint(event, \"Oggetto event\");
									}
								});
							},1000);
						}
					}

                   );

                });

                function removeGalleryImg(elm) {
                    
                    var img_id = $(elm).closest('.picture-item').data('id');
                    var post_data = {
                        img_id: img_id
                    }
                    var route = $(elm).data('delete_url');
                    page_loader();
                    setTimeout(function() {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')
                            },
                            url : route,
                            type: 'delete',
                            data: post_data,
                            dataType: \"json\",
                            success: function(response) {
                                codePrint(response, \"Response\");
                                $(elm).closest('.picture-item').closest('.ui-sortable-handle').remove();
                                page_loader_close();
                            },
                            error: function(event) {
                                
                                swal.fire({
                                    title: event.responseJSON.message,
                                    text: '',
                                    type: 'warning'
                                });
                                page_loader_close();
                            }
                        });
                    },1000);
                }
            </script>
		";
            }
        $this->addHtml($html);
        $this->printHtml();

    }

    public function saveMediaFile($file, $folder, $allowed = NULL, $filename = NULL) {

        $ResponseService = new ResponseService();

        $fileInfo = pathinfo($file->getClientOriginalName());
        $mymetype = $file->getMimeType();
        //dd([$mymetype, $fileInfo, ]);
        if(is_null($filename)) {
            $filename = time()."_".\Str::slug($fileInfo['filename']).".".$fileInfo['extension'];
        }
        if(in_array($mymetype, $this->imagesMymeTypes)) {
            $filePath = ImageService::save_image($folder, $file, $filename);
            if($filePath === false) {
                $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_image'), 'status'=>500, ]);
                return $ResponseService::getResponse();
            }
        }
        $Media = new Media();
        $Media->path = $filePath;
        $Media->original_filename = $file->getClientOriginalName();
        $Media->file_type = $mymetype;
        $Media->file_extension = $fileInfo['extension'];
        if(!$Media->save()) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }

        $ResponseService::setResponse(['result'=>true, 'data'=>$Media, 'status'=>200, ]);
        return $ResponseService::getResponse();

    }

    public function saveMediaGallery($file, $model, $modelId, $folder, $target='photo') {
           
        $saveMedia = $this->saveMediaFile($file, $folder);
        if(!$saveMedia['result']) {
            return $saveMedia;
        }

        $ResponseService = new ResponseService();

        $Media = $saveMedia['data'];
        
        $MediaGallery = new MediaGallery();
        $MediaGallery->media_id = $Media->id;
        $MediaGallery->model = $model;
        $MediaGallery->model_id = $modelId;
        $MediaGallery->target = $target;
        $MediaGallery->order = 0;
        if(!$MediaGallery->save()) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media_gallery'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }

        $Position = new PositionService($MediaGallery, ["model"=>$model, "model_id"=>$modelId, ]);
        $MaxPosition = $Position->MinMaxPosition("MAX");
        $MaxPosition++;
        $MediaGallery->order = $MaxPosition;

        if(!$MediaGallery->save()) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media_gallery'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }

        $ResponseService::setResponse(['result'=>true, 'status'=>200, ]);
        return $ResponseService::getResponse();
        
    }

    public function removeItemFromGallery(int $id, string $model, int $model_id) {
        $ResponseService = new ResponseService();
        if(!MediaGallery::where("id", $id)->where("model", $model)->where("model_id", $model_id)->delete()) {
            $ResponseService::setResponse(['error'=>__('acme:media.errors.cant_remove_media_gallery_item'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }
        $ResponseService::setResponse(['result'=>true, 'status'=>200, ]);
        return $ResponseService::getResponse();
    }

    public function saveCdnResource(string $path, string $model, int $model_id, int $order=0, $target='photo') {

        $ResponseService = new ResponseService();
        /*
        if(!file_exists($path)) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.resource_not_found'), 'status'=>404, ]);
            return $ResponseService::getResponse();
        }
        */
        $headers = @get_headers($path);
        if($headers === false) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.resource_not_found'), 'status'=>404, ]);
            return $ResponseService::getResponse();
        }
        $httpStatus = intval(substr($headers[0], 9, 3));
        if($httpStatus>=400) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.resource_not_found'), 'status'=>404, ]);
            return $ResponseService::getResponse();
        }

        $fileInfo = pathinfo($path);
        $tmpfname = tempnam(storage_path('app/public/tmp'), "tmp_");
        $file_content = file_get_contents($path);
        file_put_contents($tmpfname, $file_content);
        $mymetype = mime_content_type($tmpfname);
        $fileName = $fileInfo['filename'].".".$fileInfo['extension'];
        unset($tmpfname);

        $Media = new Media();
        $Media->path = $path;
        $Media->original_filename = $fileName ;
        $Media->file_type = $mymetype;
        $Media->file_extension = $fileInfo['extension'];
        if(!$Media->save()) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }

        $MediaGallery = new MediaGallery();
        $MediaGallery->media_id = $Media->id;
        $MediaGallery->model = $model;
        $MediaGallery->model_id = $model_id;
        $MediaGallery->target = $target;
        if ($MediaGallery->getConnection()
            ->getSchemaBuilder()
            ->hasColumn($MediaGallery->getTable(), 'is_cdn')) {
                $MediaGallery->is_cdn = true;
        }
        
        $MediaGallery->order = $order;
        if(!$MediaGallery->save()) {
            $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media_gallery'), 'status'=>500, ]);
            return $ResponseService::getResponse();
        }

        if($order == 0) {
            $Position = new PositionService($MediaGallery, ["model"=>$model, "model_id"=>$model_id, ]);
            $MaxPosition = $Position->MinMaxPosition("MAX");
            $MaxPosition++;
            $MediaGallery->order = $MaxPosition;

            if(!$MediaGallery->save()) {
                $ResponseService::setResponse(['error'=>__('acme::media.errors.cant_save_media_gallery'), 'status'=>500, ]);
                return $ResponseService::getResponse();
            }
        }
        

        $ResponseService::setResponse(['result'=>true, 'status'=>200, ]);
        return $ResponseService::getResponse();

    }

}