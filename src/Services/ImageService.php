<?php
namespace Kikero\Services;

use Storage;
use File;
use Kikero\Services\CropImage;

class ImageService {

    protected $path;
    protected $filename;
    protected $sizes;
    protected $pathimg;

    public function __construct() {
        $this->sizes = \Config::get("project.image_sizes", [2000, 1200, 600, "crop", ]);
    }

    public function save_image(string $folder, $file, $filename) {
        // $folder = "locations/5";
        if(!$this->isValidImage($file)) return false;

        $this->path = $folder;

        $folder_elms = explode("/", $folder);
        $storage_directory = public_path("/");
        foreach($folder_elms as $elm) {
            $storage_directory .= $elm;
            //File::isDirectory($storage_directory) or File::makeDirectory($storage_directory, 0755, true, true);
        }

        $this->filename = $filename;
        $pathImg = $file->storeAs("public/".$this->path, $this->filename);
        $originalFileSize = getimagesize($file);
        $CropImage = new CropImage();
        $CropImage->openImg(public_path("/storage/").$this->path."/".$this->filename);

        foreach($this->sizes as $size) {
            if(is_integer($size)) {
                $width = $size;
                $height = ($originalFileSize[1]*$width) / $originalFileSize[0];
                $CropImage->creaThumb($width, $height);
                $CropImage->saveThumb(public_path("/storage/").$this->path."/".$size."_".$filename);
                $CropImage->setThumbAsOriginal();
            } else {
                if($size == "crop") {
                    $width = 800;
                    $height = 800;
                    
                    $x = 0;
                    $y = 0;
                    if($originalFileSize[0]>$originalFileSize[1]) {
                        $width = $CropImage->getRightWidth($width);
                        $x=($width-$height)/2;
                    }
                    if($originalFileSize[1]>$originalFileSize[0]) {
                        $height = $CropImage->getRightWidth($height);
                        $y=($height-$width)/2;
                    }

                    $CropImage->creaThumb($width, $height);
                    $CropImage->setThumbAsOriginal();
                    $CropImage->cropThumb(800, 800, $x, $y);
                    $CropImage->saveThumb(public_path("/storage/").$this->path."/crop_".$filename);
                }
            }
        }

        return $this->path."/".$filename;

    }

    public function isValidImage($file):bool {
        $allowedMimeTypes = ['image/jpeg','image/gif','image/png', ];
        $mymetype = $file->getMimeType();
        if(in_array($mymetype, $allowedMimeTypes)) {
            return true;
        }
        return false;
    }
	
	public function getImagePath(string $path, $size="") {
		$explode = explode("/", $path);
		$filename = $explode[count($explode)-1];
		if(!empty($size)) {
			$filename = (string)$size."_".$filename;
		}
		$explode[count($explode)-1] = $filename;
		$filename = implode("/", $explode);

		return asset("storage/".$filename);
	}

    /**
     * Converte un'immagine passata come url nel formato base64 da passare all'src di una img
     */
    public static function getBase64Image($imageUrl) {
        $stream_opts = [
            "ssl" => [
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ]
        ];
        $imageData = file_get_contents($imageUrl, false, stream_context_create($stream_opts));
        return "data:imge/png;base64,".base64_encode($imageData);
    }

}