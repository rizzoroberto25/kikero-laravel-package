<?php

namespace Kikero\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class Media extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $table = "media";

    public function setMediaTable($table):bool {
        if(!Schema::hasTable($table)) return false;
        $this->table = $table;
        return true;
    }
}