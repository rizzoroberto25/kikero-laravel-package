<?php

namespace Kikero\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class MediaGallery extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $table = "media_gallery";

    public function setMediaTable($table):bool {
        if(!Schema::hasTable($table)) return false;
        $this->table = $table;
        return true;
    }

    public function media() {
        return $this->belongsTo(\Kikero\Models\Media::class);
    }
}