<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\PositionService;

class PositionServiceFacade extends Facade {
    protected static function getFacadeAccessor() { 
        return 'positionservice';
    }
}