<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\Utility;

class UtilityServiceFacade extends Facade {
    protected static function getFacadeAccessor() { 
        return 'utility';
    }
}