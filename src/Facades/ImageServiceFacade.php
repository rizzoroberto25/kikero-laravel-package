<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\ImageService;

class ImageServiceFacade extends Facade {
    protected static function getFacadeAccessor() { 
        return 'imageservice';
    }
}