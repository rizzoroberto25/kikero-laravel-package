<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\TextManager;

class TextManagerFacade extends Facade {
    protected static function getFacadeAccessor() { 

        //return TextManager::class;
        return 'textmanager';
    }
}