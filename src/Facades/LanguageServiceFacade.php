<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\LanguageService;

class LanguageServiceFacade extends Facade {
    protected static function getFacadeAccessor() { 

        return 'languageservice';
    }
}