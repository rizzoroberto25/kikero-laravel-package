<?php
namespace Kikero\Facades;

use Illuminate\Support\Facades\Facade;
use Kikero\Services\MediaService;

class MediaServiceFacade extends Facade {
    protected static function getFacadeAccessor() { 
        return 'mediaservice';
    }
}