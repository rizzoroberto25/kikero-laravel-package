<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMediaGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_gallery', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("media_id")->unsigned();
            $table->foreign("media_id")->references("id")->on("media");
            $table->string('model')->index();
            $table->bigInteger('model_id')->index();
            $table->integer("order")->unsigned()->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_gallery');
    }

}