<?php
return [
    "gallery_title" => "Galleria",
    "gallery_insert_images" => "Inserisci foto in Gallery",
    "no_img_in_gallery" => "Nessun immagine in Gallery",
    "save_gallery" => "Salva Gallery",
    "media_gallery_item_removed" => "Il media è stato rimosso dalla Galleria",
    "insert_youtube_iframe" => "Incolla l'embed di Youtube",

    "errors" => [
        "cant_create_media" => "Errore scrittura del file nel filesystem",
        "cant_save_media" => "Errore salvataggio media in DB",
        "cant_update_positions" => "Errore salvataggio posizioni",
        "cant_remove_media_gallery_item" => "Errore nella cancellazione media dalla Galleria",
        "resource_not_found" => "Risorsa non trivata",
    ],

];