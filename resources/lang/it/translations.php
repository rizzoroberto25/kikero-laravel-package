<?php
return [
    "errors" => [
        "no_translations" => "Nessun testo trovato nel DB",
		"cant_delete_translations" => "Errore nella cancellazione delle traduzioni",
    ],
];